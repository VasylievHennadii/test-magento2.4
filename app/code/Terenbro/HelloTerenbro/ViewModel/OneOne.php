<?php
namespace Terenbro\HelloTerenbro\ViewModel;

use Magento\Framework\Exception\CouldNotSaveException;

class OneOne implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var null|\Terenbro\HelloTerenbro\Model\Team
     */
    private $model = null;

    /**
     * @var \Terenbro\HelloTerenbro\Api\TeamRepositoryInterface|\Terenbro\HelloTerenbro\Model\TeamRepository
     */
   protected $teamRepository;

    /**
     * @var \Magento\Framework\Api\SearchCriteriaBuilder
     */
    protected $searchCriteriaBuilder;

    /**
     * @param \Terenbro\HelloTerenbro\Api\TeamRepositoryInterface $teamRepository
     */
    public function __construct(
        \Terenbro\HelloTerenbro\Api\TeamRepositoryInterface $teamRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ){
        $this->teamRepository = $teamRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return string
     */
    public function iAmViewModel()
    {
        return 'i Am ViewModel';
    }

    /**
     * @return string
     */
    public function getText($arg1, $arg2)
    {
        $text = 'Terenbro team member';
        return $arg1 . ' - Terenbro team members - ' . $arg2;
    }

    /**
     * @param string $name
     * @param integer $age
     * @param string $heading
     * @param string $comment
     */
    public function saveNewMember($name, $age, $heading, $comment)
    {
        $model = $this->teamRepository->getEmtyModel();

        $model->setName($name)
            ->setAge($age)
            ->setHeading($heading)
            ->setComment($comment);

        try {
            $this->teamRepository->save($model);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__("Failed to save new team member. Sorry..."));
        }



//        $model = $this->teamFactory->create();
//
//        $model->setData('name', $name)
//            ->setData('age', $age)
//            ->setData('heading', $heading)
//            ->setData('comment', $comment);
//
//        $this->teamResourceModel->save($model);
    }

    /**
     * Get collection of all members from 'team_terenbro'
     *
     * @return \Magento\Framework\DataObject[]|\Terenbro\HelloTerenbro\Model\ResourceModel\Collection\Team[]
     */
    public function getAllMembers()
    {
        $searchCriteria = $this->searchCriteriaBuilder->addFilter(
            \Terenbro\HelloTerenbro\Api\Data\TeamInterface::FIELD_NAME_ID,
            3,
            'eq'
        )->create();

        $searchResults = $this->teamRepository->getList($searchCriteria);

        var_dump($searchResults->getItems());


//        $collection = $this->teamCollectionFactory->create();

//      'equal' => 'Igor Bezeka'     ||   'name' === 'Igor Bezeka'
    //  $collection->addFieldToFilter('name', ['eq' => 'Igor Bezeka']);

//      'equal' => 25     ||   'age' === 25
//        $collection->addFieldToFilter('age', ['eq' => 25]);

//      'not equal' => 7    ||   'id' !== 7
//      $collection->addFieldToFilter('id', ['neq' => 7]);

//      'greater then equal' => 4    ||   'id' >= 4
//      $collection->addFieldToFilter('id', ['gteq' => 4]);

//      'lower then equal' => 6    ||   'id' <= 6
//      $collection->addFieldToFilter('id', ['lteq' => 6]);

//        $collection->load();

//        return $collection->getItems();
    }

    /**
     * Return name of the member
     *
     * @param int $id
     * @return string
     */
    public function getMemberName($id)
    {
        try {
            return $this->teamRepository->getById($id)->getName();
        } catch (\Exception $e){
            return 'Sorry. Member with this name not found =)';
        }

//        return $this->getMemberModel($id)->getData('name');
    }

    /**
     * Return age of the member
     *
     * @param int $id
     * @return string
     */
    public function getMemberAge($id)
    {
        try {
            return $this->teamRepository->getById($id)->getAge();
        } catch (\Exception $e){
            return 'Sorry. Member with this age not found =)';
        }

//        return $this->getMemberModel($id)->getData('age');
    }

    /**
     * Return model if exists.
     * IF not, load from db and then return.
     *
     * @param integer $id
     * @return \Terenbro\HelloTerenbro\Model\Team|null
     */
    private function getMemberModel($id)
    {
        if ($this->model === null ) {
            // Creates new instance of the 'members object'. A.K.A. individual line/row from mysql table.
            $model = $this->teamFactory->create();

            // Loads the data from the database and puts it to the $model variable.
            $this->teamResourceModel->load($model, $id);

            // Save model to the class, so that same model won't be loaded numerous times.
            $this->model = $model;
        }

        return $this->model;
    }

}
