<?php
namespace Terenbro\HelloTerenbro\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddDataToTheTable implements DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    protected $moduleDataSetup;

    /**
     * @var \Terenbro\HelloTerenbro\Model\TeamFactory
     */
    protected $model;

    /**
     * @var \Terenbro\HelloTerenbro\Model\ResourceModel\Team
     */
    protected $resourceModel;

    /**
     * AddDataToTheTable
     *
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     * @param \Terenbro\HelloTerenbro\Model\TeamFactory $teamModel
     * @param \Terenbro\HelloTerenbro\Model\ResourceModel\Team $teamResourceModel
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Terenbro\HelloTerenbro\Model\TeamFactory $teamModel,
        \Terenbro\HelloTerenbro\Model\ResourceModel\Team $teamResourceModel
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->model = $teamModel;
        $this->resourceModel = $teamResourceModel;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    /**
     *
     * Applies this function only once.
     *
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $model = $this->model->create();

        $model->setData('name', 'Hennadii')
            ->setData('age', 40)
            ->setData('heading', 'Junior Magento 2 Developer')
            ->setData('comment', 'He is a cool guy');

        $this->resourceModel->save($model);

        $this->moduleDataSetup->endSetup();
    }
}
