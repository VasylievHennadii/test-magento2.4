<?php
namespace Terenbro\HelloTerenbro\Model;

use Terenbro\HelloTerenbro\Api\Data\TeamInterface;

class Team extends \Magento\Framework\Model\AbstractModel implements TeamInterface
{
    /**
     * @inheirtDoc
     */
    public function getId()
    {
        return $this->setData(self::FIELD_NAME_ID);
    }

    /**
     * @inheirtDoc
     */
    public function getAge()
    {
        return $this->getData(self::FIELD_NAME_AGE);
    }

    /**
     * @inheirtDoc
     */
    public function setAge($age)
    {
        return $this->setData($age, self::FIELD_NAME_AGE);
    }

    /**
     * @inheirtDoc
     */
    public function getName()
    {
        return $this->getData(self::FIELD_NAME_NAME);
    }

    /**
     * @inheirtDoc
     */
    public function setName($name)
    {
        return $this->setData($name, self::FIELD_NAME_NAME);
    }

    /**
     * @inheirtDoc
     */
    public function getHeading()
    {
        return $this->getData(self::FIELD_NAME_HEADING);
    }

    /**
     * @inheirtDoc
     */
    public function setHeading($heading)
    {
        return $this->setData($heading, self::FIELD_NAME_HEADING);
    }

    /**
     * @inheirtDoc
     */
    public function getComment()
    {
        return $this->getData(self::FIELD_NAME_COMMENT);
    }

    /**
     * @inheirtDoc
     */
    public function setComment($comment)
    {
        return $this->setData($comment, self::FIELD_NAME_COMMENT);
    }

    /**
     * @inheirtDoc
     */
    public function getCreatedAt()
    {
        return $this->getData(self::FIELD_NAME_CREATED_AT);
    }

    /**
     * @inheirtDoc
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::FIELD_NAME_UPDATED_AT);
    }

    /**
     * @inheirtDoc
     */
    protected function _construct()
    {
        $this->_init(\Terenbro\HelloTerenbro\Model\ResourceModel\Team::class);
    }
}
