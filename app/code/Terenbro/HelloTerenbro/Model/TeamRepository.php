<?php
namespace Terenbro\HelloTerenbro\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

class TeamRepository implements \Terenbro\HelloTerenbro\Api\TeamRepositoryInterface
{
    /**
     * @var \Terenbro\HelloTerenbro\Model\TeamFactory
     */
    protected $teamFactory;

    /**
     * @var \Terenbro\HelloTerenbro\Model\ResourceModel\Team
     */
    protected $teamResourceModel;

    /**
     * @var \Terenbro\HelloTerenbro\Model\ResourceModel\Collection\TeamFactory
     */
    protected $teamCollectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var SearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * OneOne constructor
     *
     * @param \Terenbro\HelloTerenbro\Model\TeamFactory $teamFactory
     * @param \Terenbro\HelloTerenbro\Model\ResourceModel\Team $teamResourceModel
     * @param \Terenbro\HelloTerenbro\Model\ResourceModel\Collection\TeamFactory $teamCollectionFactory
     * @param \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor
     * @param \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        \Terenbro\HelloTerenbro\Model\TeamFactory $teamFactory,
        \Terenbro\HelloTerenbro\Model\ResourceModel\Team $teamResourceModel,
        \Terenbro\HelloTerenbro\Model\ResourceModel\Collection\TeamFactory $teamCollectionFactory,
        \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface $collectionProcessor,
        \Magento\Framework\Api\SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->teamFactory = $teamFactory;
        $this->teamResourceModel = $teamResourceModel;
        $this->teamCollectionFactory = $teamCollectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @inheritDoc
     */
    public function save($model)
    {
        try {
            return $this->teamResourceModel->save($model);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__("Save failed. Sorry..."));
        }
    }

    /**
     * @inheritDoc
     */
    public function delete($model)
    {
        try {
            $this->teamResourceModel->delete($model);
            return true;
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__("Failed to delete. Sorry..."));
        }
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id)
    {
        try {
            $this->teamResourceModel->delete($this->getById($id));

            return true;
        } catch (\Exception $e) {
            throw new CouldNotDeleteException(__("Failed to delete. Sorry..."));
        }
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        try {
            $model = $this->teamFactory->create();
            $this->teamResourceModel->load($model, $id);

            return $model;
        } catch (\Exception $e) {
            throw new NoSuchEntityException(__("No element with this id. Sorry..."));
        }
    }

    /**
     * @inheritDoc
     */
    public function getList($searchCriteria)
    {
        $collection = $this->teamCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResult = $this->searchResultsFactory->create();

        $searchResult->setSearchCriteria($searchCriteria)
            ->setTotalCount($collection->getSize())
            ->setItems($collection->getItems());

        return $searchResult;
    }

    /**
     * @return Team
     */
    public function getEmtyModel()
    {
        return $this->teamFactory->create();
    }
}
