<?php
namespace Terenbro\HelloTerenbro\Model\ResourceModel\Collection;


class Team extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Terenbro\HelloTerenbro\Model\Team::class,
            \Terenbro\HelloTerenbro\Model\ResourceModel\Team::class
        );
    }

}
