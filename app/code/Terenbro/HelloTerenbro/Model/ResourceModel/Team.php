<?php
namespace Terenbro\HelloTerenbro\Model\ResourceModel;

use Terenbro\HelloTerenbro\Api\Data\TeamInterface;

class Team extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init(TeamInterface::TABLE_NAME, TeamInterface::FIELD_NAME_ID);
    }
}
