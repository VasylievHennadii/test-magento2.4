<?php


namespace Terenbro\HelloTerenbro\Block;


class Terenbro extends \Magento\Framework\View\Element\Template
{
    public function __construct(\Magento\Framework\View\Element\Template\Context $context)
    {
        parent::__construct($context);
    }

    public function outputHelloTerenbro()
    {
        return 'Output text using function outputHelloTerenbro from the Block';
    }


}
