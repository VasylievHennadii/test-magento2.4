<?php
namespace Terenbro\HelloTerenbro\Controller\Index;

use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $page;
    }

//    protected $_pageFactory;
//    public function __construct(
//        \Magento\Framework\App\Action\Context $context,
//        \Magento\Framework\View\Result\PageFactory $pageFactory)
//    {
//        $this->_pageFactory = $pageFactory;
//        return parent::__construct($context);
//    }

//    /**
//     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
//     */
//    public function execute()
//    {
//        return $this->_pageFactory->create();
//    }

}
