<?php
namespace Terenbro\HelloTerenbro\Api;

use Magento\Framework\Exception\CouldNotDeleteException;

interface TeamRepositoryInterface
{
    /**
     * @param \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team $model
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function save($model);

    /**
     * @param \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team $model
     * @return true
     * @throws CouldNotDeleteException
     */
    public function delete($model);

    /**
     * @param integer $id
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function getById($id);

    /**
     * @param integer $id
     * @return true
     * @throws CouldNotDeleteException
     */
    public function deleteById($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteria $searchCriteria
     * @return \Magento\Framework\Api\SearchResultsInterface
     */
    public function getList($searchCriteria);
}
