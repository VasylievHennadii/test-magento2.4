<?php
namespace Terenbro\HelloTerenbro\Api\Data;

interface TeamInterface
{
    const TABLE_NAME = 'team_terenbro';

    const FIELD_NAME_ID = 'id';
    const FIELD_NAME_AGE = 'age';
    const FIELD_NAME_NAME = 'name';
    const FIELD_NAME_HEADING = 'heading';
    const FIELD_NAME_COMMENT = 'comment';
    const FIELD_NAME_CREATED_AT = 'created_at';
    const FIELD_NAME_UPDATED_AT = 'updated_at';

    /**
     * @return integer
     */
    public function getId();

    /**
     * @return string|integer
     */
    public function getAge();

    /**
     * @param string|integer $age
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function setAge($age);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getHeading();

    /**
     * @param string $heading
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function setHeading($heading);

    /**
     * @return string
     */
    public function getComment();

    /**
     * @param string $comment
     * @return \Terenbro\HelloTerenbro\Api\Data\TeamInterface|\Terenbro\HelloTerenbro\Model\Team
     */
    public function setComment($comment);

    /**
     * @return string
     */
    public function getCreatedAt();

    /**
     * @return string
     */
    public function getUpdatedAt();
}


