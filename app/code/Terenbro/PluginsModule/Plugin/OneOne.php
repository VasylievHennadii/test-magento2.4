<?php
namespace Terenbro\PluginsModule\Plugin;


class OneOne
{
    public function aroundGetText(\Terenbro\HelloTerenbro\ViewModel\OneOne $subject, \Closure $proceed, $arg1, $arg2)
    {
        //Before plugin

        $originalFunctionResult = $proceed($arg1, $arg2);

        //After plugin

        return 'This was from plugin' . "\n";
    }
}
