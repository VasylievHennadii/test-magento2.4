<?php
namespace Terenbro\PluginsModule\Plugin;


class Team
{
    public function afterGetName(\Terenbro\HelloTerenbro\Api\Data\TeamInterface $team, $result)
    {
        if($result === 'Lena Volinskaya'){
            return  $result . ' - cool girl';
        }else{
            return  $result . ' - cool guy';
        }
    }

    public function beforeGetName(\Terenbro\HelloTerenbro\Api\Data\TeamInterface $team, ...$args)
    {
        return $args;
    }
}
